package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ControllerImpl implements Controller {

    private List<String> list = new ArrayList<>();
    private Iterator<String> iter = list.iterator();

    /**@param s
     * 
     * Add a string to the list.
     */
    public void setNextString(final String s) {
        list.add(s);
    }

    /**
     * Get the String from list.
     * 
     * @return nextString.
     */
    public String getNextString() {
        return iter.next();
    }

    /**
     * Get the history.
     * 
     * @return list
     */
    public List<String> getHistory() {
        return list;
    }

    /**
     * @param s
     * 
     * @throws IllegalStateException
     * 
     * Print the current string.
     */
    public void printString(final String s) throws IllegalStateException {
        System.out.println(s);
    }

}
